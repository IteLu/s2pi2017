/*
 * =====================================================================================
 *
 *       Filename:  omp_vector.c
 *
 *    Description:  Vectorization with OpenMP example
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

////////////////////////////////
// Vectorization of a = B * c //
///////////////////////////////

uint64_t rdtsc(){
  unsigned int lo,hi;
  __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
  return ((uint64_t)hi << 32) | lo;
}

int main()
{
  int n,i,j;
  double flops;
  int N = 2000;

  struct timeval start, end;
  float delta;

  uint64_t cycles;

  double a[N],B[N][N],c[N];

  // Initialize vectors a, c and matrix b
  for(i = 0; i < N; i++) {
    a[i] = 0.0;
    c[i] = 1.0;
    for(j = 0; j < N; j++) {      
	B[i][j] = 2.0;
    }
  }

  gettimeofday(&start, NULL);
  cycles = rdtsc();

  // Compute a = B * c
  for (n = 0; n < 10000; n++) {
#pragma omp parallel for private(i,j)
    for(i = 0; i < N; i++)      
      for(j = 0; j < N; j++)
	a[i] += B[i][j]*c[j];
  }

  cycles = rdtsc() - cycles ;
  gettimeofday(&end, NULL);
  delta = ((end.tv_sec-start.tv_sec)*1000000u + end.tv_usec-start.tv_usec)/1.e6;
  flops = N*N*2;

  printf("run time    = %fs\n", delta);
  printf("flops/cycle = %f\n", 10000*flops/cycles);
  printf("GFLOPS       = %f\n", 10000*flops/delta/1.e9);

  double sum = 0;
  for(i = 0; i < N; i++) 
    sum += a[i];
  printf("sum of a    = %f\n", sum);

  return 0;
}
