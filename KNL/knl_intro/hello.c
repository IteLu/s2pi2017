/*
 * =====================================================================================
 *
 *       Filename:  hello.c
 *
 *    Description:  Typical hello world code
 *
 *        Version:  1.0
 *        Created:  11/10/2016 08:25:45 AM
 *       Revision:  none
 *         Author:  Antonio Gomez
 *   Organization:  TACC
 *
 * =====================================================================================
 */

#include <stdio.h>

int main (void)
{
  puts ("Hello World!");
  return 0;
}
