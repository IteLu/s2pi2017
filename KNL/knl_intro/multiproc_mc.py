#!/usr/bin/env python
"""
=====================================================================================

         Filename:  multiproc_mc.py
 *
 *    Description:  Monte Carlo integration using python multiprocessing module
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */
"""
# multiproc_mc.py
#integrate f(x) from a to b

from multiprocessing import Pool
from numpy import random
import time,math,sys

def f(x):
    r,s=x
    var = random.random()*(s-r)+r # [0,1] -> [a,b]
    return var**2 # x^2

a = 0 # lower integration bound
b = 1 # upper integration bound
N = 10000000 # number of samples
irange = N*[(a,b)]

print 'Start MC integration using ' + str(sys.argv[1]) + ' processes'

# Multiprocessing
p = Pool(processes=int(sys.argv[1]))
random.seed(0) 
start = time.time()
samples = p.map(f,irange) # parallel map
end = time.time()
I = (b-a)*sum(samples)/N # compute integral
print 'result = ', I
print 'runtime = ' + str(end-start) +'s'


