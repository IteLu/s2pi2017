#!/bin/bash -l
#SBATCH -N 2
#SBATCH -t 5
#SBATCH -L SCRATCH
#SBATCH -C haswell
#SBATCH -J ex6
##SBATCH --reservation=STPI_hsw

# For this exercise, we have added some errors to our miniFE source code that
# will crash or deadlock the code during the run. We'll run at a high(er) 
# process count than is easy to debug without proper tools, and use ATP to try 
# to find the location of the bug(s)

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}

module load atp
export ATP_ENABLED=1

# In this exercise, we will run a small miniFE job at a variety of scales
# and plot the time and speedup against the number of MPI ranks. For now,
# we'll stay within a single node.

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex6-debugging/build/miniFE.x

# A 200x200x200 miniFE job should finish within 5 minutes on a single core 
# of a Cori Haswell node:
sz=200
cmd="$ex -nx $sz -ny $sz -nz $sz"

# A Cori Haswell node has 32 cores, each with 2 hyperthreads. Our Slurm 
# setup considers each hyperthread to be a CPU, but for this exercise we want
# each MPI task to have its own core. The following recipe calculates how
# many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
# a "cpu" in Slurm is a hyperthread:
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((SLURM_CPUS_ON_NODE/cpus_per_task))
max_mpi_ranks=$((SLURM_NNODES * max_mpi_per_node))

nranks=$max_mpi_ranks

# make the run output easy to identify:
label=sz${sz}-${SLURM_NNODES}n-${nranks}mpi-$SLURM_JOB_ID
# run in a unique directory under $SCRATCH:
rundir=$SCRATCH/$training_dir/ex6-debugging/$label
mkdir -p $rundir
cd $rundir

srun -n$nranks -c$cpus_per_task --cpu_bind=cores $cmd > stdout 2> stderr

