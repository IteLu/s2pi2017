Scaling to Petascale Institute - Scaling and Profiling Workshop
===============================================================

Exercise 1: Observing scaling behavior of miniFE
------------------------------------------------

In this exercise we will build miniFE and run it with 1, 2, 4, etc MPI ranks, 
up to full node. We assume you have some familiarity with building code and 
with running batch jobs on an HPC system, but not necessarily on Cori, 
Blue Waters or Stampede. Some useful documentation for running jobs on each
of these can be found at:

Cori:
  http://www.nersc.gov/users/computational-systems/cori/running-jobs/

Blue Waters:
  https://bluewaters.ncsa.illinois.edu/running-your-jobs

Stampede:
  https://portal.tacc.utexas.edu/user-guides/stampede#running-jobs

**Note:** In the command samples here, ``$`` represents the bash prompt. Lines
not beginning with ``$`` are sample outputs from commands. ``...`` generally 
indicates that the output has been cut for brevity.

.. _Step 1:

Step 1: Build miniFE
^^^^^^^^^^^^^^^^^^^^

``cd`` into the ``build`` directory and use your favorite editor to look at the 
``Makefile`` there. (If you like, take a look also at the ``make_targets`` file 
it includes)::

  $ cd build

  $ cat Makefile
  # C++ and C compilers:
  CXX = CC
  CC = cc
  ...
  include ../../miniFE/source/make_targets

  $ less ../../miniFE/source/make_targets
  ...

On a Cray system such as Cori or Blue Waters, compiler wrappers such as CC and 
cc invoke an underlying compiler. By default the Intel compilers are called on
Cori and the Cray compilers are called on Blue Waters. The Makefiles are set up
to use the Intel compilers so on Blue Waters, swap to the Intel PrgEnv::

  $ module swap PrgEnv-cray PrgEnv-intel

Alternatively, you can adjust the options in ``Makefile`` to suit the compilers 
on your system.

Now build the executable::

  $ make

If everything worked, you should see an executable ``miniFE.x`` in your current
(``build``) directory.


.. _Step 2:

Step 2: Run miniFE on 1, 2, 4, etc MPI ranks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``cd ..`` back to this directory and take a look in ``cori-jobscript.sh``. (If
you're comfortable using bash at a slightly more advanced level, and plan to try
the exercise extension, you can look at ``cori-jobscript-advanced.sh`` instead).
For Blue Waters, use ``bw-jobscript.sh`` instead.

The script actually runs a sequence of job steps. Each step will run the 
miniFE.x you created in `Step 1`_, with an increasing number of MPI ranks. Each
run is in a unique subdirectory under the ``$SCRATCH`` filesystem. The time
taken by each run is captured in a ``timings-<job_id>.dat`` file in this 
directory.

Submit the job with::

  $ sbatch cori-jobscript.sh
  Submitted batch job 5381580

Or::

  $ qsub bw-jobscript.sh
  Job submitted to account: bako
  7036536.bw

You should see a message like the one above (Where ``5381580`` is replaced by 
the job id of your job)

On Cori, you can check the progress of your job with ``sqs``. If the ``ST`` 
field shows ``PD``, your job is still in the queue. ``R`` indicates it is now 
running, and when the job completes it will disappear from ``sqs`` output.

When the job completes you should see two files in the directory you submitted
from: one called something like ``slurm-5381580.out`` or ``ex1.o7036536`` and 
another like ``timings-5381580.dat`` (again, with ``5381580`` replaced by your 
job id). If you look in your ``$SCRATCH`` (or ``$HOME/scratch``) directory you 
should also find a series of folders with names like ``sz200-1n-8mpi-5381580``, 
each has the output from one of the miniFE runs.


.. _Step 3:

Step 3: Plot the timings against number of MPI ranks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Take a look at ``timings-5381580.dat``::

  $ cat timings-5381580.dat
  #nranks    bm_time    walltime
     1       113.062    118.228
     2       56.0979    61.809
     4       29.5361    36.156
     8       18.2173    22.852
     16      11.9747    15.306
     32      9.55725    17.514

There are two columns of results, "bm_time" being the time in seconds as 
reported by miniFE and "walltime" the total time taken by the srun command
in ``cori-jobscript.sh``. The walltime will include time taken to launch the 
job on all nodes while the bm_time is only the time used by the benchmark 
itself - the decision of which timing to use depends on your purposes.

For now, we'll plot both but look mostly at the walltime.

You can plot the time against number of ranks in whichever charting tool you
prefer (matplotlib, a spreadsheet, ..?). These instructions use gnuplot, which
is readily available on most Linux systems. 

To best view the plots you will need an X connection - by running an X server 
such as XQuartz or Xming on your laptop, and by using ``ssh -X ...`` to login
to the HPC system. If this is not possible you have two other options:

- you can instruct gnuplot to draw to a file, then ``scp`` it back to your 
  laptop to view.
- you can instruct gnuplot to draw an ascii-art version of the plot on the 
  terminal (with much clunkier resolution, of course)

Start gnuplot and tell it the name of the file containing fdata you wish to 
plot::

  $ gnuplot
  gnuplot> f="timings-5381580.dat"

If you wish to plot to a PNG file instead of the screen (don't do this if you 
want to draw directly on the screen!)::

  gnuplot> set term png
  gnuplot> set output "ex1-plot1.png"

Or if you wish to plot to the terminal::

  gnuplot> set term dumb

Now plot the timings (note the ``\`` for line continuation, there must be no
space after it, and you begin pasting again after the ``>`` prompt on the next
line)::

  gnuplot> set xlabel "nranks"
  gnuplot> set ylabel "sec"
  gnuplot> plot f using 1:2 with linespoints title "BM time", \
  > f using 1:3 with linespoints title "Walltime"

We can do that plot with a lot less typing::

  gnuplot> plot f u 1:2 w lp t "BM time", f u 1:3 w lp t "Walltime"

So, did it scale well? It's not very obvious yet so we'll add a line showing 
what the ideal timing would be. The ideal timing is the time for one process
divded by the number of processes. We'll use this neat little function 
(courtesy of https://stackoverflow.com/questions/14033668/how-to-manipulate-data-with-gnuplots-plot-with-a-number-stored-in-the-same-file)
to capture the first value in any column of the data file::

  gnuplot> first(x) = ($0 == 0) ? (first = x, 1/0) : first
  gnuplot> plot f u 1:2 w lp t "BM time", f u 1:3 w lp t "Walltime", \
  >        f u 1:(first($3)/$1) w lp t "ideal"

Our scaling is clearly worse than ideal, but a linear plot of time against 
number of processors isn't the best way to show parallel performance. A log 
scale plot is better, because now the ideal performance is a straight line::

  gnuplot> set logscale
  gnuplot> plot f u 1:2 w lp t "BM time", f u 1:3 w lp t "Walltime", \
  >        f u 1:(first($3)/$1) w lp t "ideal"

Another option is to plot the speedup, that is time(1proc)/time(nproc)::

  gnuplot> speedup(x) = first(x)/x
  gnuplot> unset logscale
  gnuplot> set ylabel "speedup"
  gnuplot> set key left top
  gnuplot> plot f u 1:(speedup($2)) w lp t "speedup(BM time)", \
  >        f u 1:(speedup($3)) w lp t "speedup(Walltime)", f u 1:1 w lp t "ideal"

Now the plot goes up and to the right, which is intuitively pleasing.

We'll discuss what these plots tell us during this session.

.. _Extensions:

Extension 1: Multi-node
^^^^^^^^^^^^^^^^^^^^^^^

If you're ahead of the group, take a look at ``cori-jobscript-advanced.sh``, 
uncomment the appropriate lines, run on 2 and perhaps 4 nodes and plot the
additional timings. What do you notice?

Extension 2: OpenMP
^^^^^^^^^^^^^^^^^^^

In ``build/Makefile`` there is an option to build for OpenMP, and 
``cori-jobscript-advanced.sh`` has options for plotting OpenMP scaling (we'll 
do more of this in `exercise 4`_). Try a few runs with a single MPI rank and 
an increasing number of OpenMP threads, what do you see? 

.. _exercise 4: file://../ex4/README.rst



